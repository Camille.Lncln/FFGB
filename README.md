# FFGB (Fromage Foie Gras Baguette) :cheese:
=====

### Project Description :paperclip:
=====

This project is part of the 5th year engineering school of EPF of JAVA language course. The instructions were to develop a web application using JAVA language, Kotlin, Spring-Boot and Hibernate technologies.
We decided to develop a platform that share advices to send french food arround the world and show a quick presentation about it for each product.

### Project architecture :hammer_and_wrench:
=====

The project is made up of:
- a database that creates/updates itself everytime you run the project.
- a frontend to provide user experience.
- a backend to provide the logic between components.

### Test the project :space_invader:
=====

- When you start the project you need to modify the "applicationproperties" file to fit your own database, then it will create the tables that the webapp needs.
- To use the webapp you need to register and to log in.
- We started the project with 10 countries so click on one of these:
  - Australia
  - Brazil
  - Canada
  - China
  - Germany
  - Italy
  - Japan
  - New Zealand
  - Marocco
  - USA
  

Thank you very much for your time reviewing this project! Enjoy your french meal! :baguette_bread: