package fr.epf.fromagefoisgrasbaguette;

import fr.epf.fromagefoisgrasbaguette.models.User;
import fr.epf.fromagefoisgrasbaguette.persistence.UserDao;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.swing.*;
import java.io.IOException;
import java.util.ArrayList;


public class Account {


    private final UserDao userDao;
    private String returnString;
    private boolean returnConnexion;


    public Account(UserDao userDao) {
        this.userDao = userDao;
    }

    //inscription
    public String getInscription(User user) {
        ArrayList<User> users = new ArrayList<>();
        userDao.findAll().forEach(users::add);
    boolean test = false;

    //check the 2 password

        if (user.getPsw2().equals(user.getPsw())){

            for(int i = 0; i<users.size(); i++){
                if(users.get(i).getEmail().equals(user.getEmail())){
                    test = true;
                }
            }
            //check if the email is unique
            if(test== true){
                JFrame parent = new JFrame();
                JOptionPane.showMessageDialog(parent, "Il y a déjà un compte associé à cet email");
                returnString = "redirect:/inscription";
            }
            else {
                userDao.save(user);
                JFrame parent = new JFrame();
                JOptionPane.showMessageDialog(parent, "Bienvenu, votre compte est crée");
                returnString = "redirect:/index";
            }
        }
        else{

            //if the inscription form is not valid
            JFrame parent = new JFrame();
            JOptionPane.showMessageDialog(parent, "Impossible de créer votre compte les mots de passe ne sont pas les mêmes");
            returnString = "redirect:/inscription";
        }
        return returnString;
    }
//connexion
    public boolean getConnexion(@RequestParam("email") String email, User user, HttpServletRequest request) throws ServletException, IOException {
        ArrayList<User> users = new ArrayList<>();
        userDao.findAll().forEach(users::add);
        boolean test = false;

        //check email & password in the database
        for (int i = 0; i < users.size(); i++) {

            if (users.get(i).getEmail().equals(user.getEmail())) {

                if (users.get(i).getPsw().equals(user.getPsw())) {
                    test = true;
                }
            }


        }
//if ok, session manager is launch
        if (test == true) {
            JFrame parent = new JFrame();
            JOptionPane.showMessageDialog(parent, "Vous êtes connecté");
            returnConnexion = true;

        } else {
            JFrame parent = new JFrame();
            JOptionPane.showMessageDialog(parent, "Vos identifiants sont incorrects");
            returnConnexion = false;

        }
return  returnConnexion;



    }


}

