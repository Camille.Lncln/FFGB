package fr.epf.fromagefoisgrasbaguette;

import fr.epf.fromagefoisgrasbaguette.models.Cheese;
import fr.epf.fromagefoisgrasbaguette.models.Country;
import fr.epf.fromagefoisgrasbaguette.models.Wine;
import fr.epf.fromagefoisgrasbaguette.persistence.CountryDao;
import fr.epf.fromagefoisgrasbaguette.persistence.CheeseDao;
import fr.epf.fromagefoisgrasbaguette.persistence.WineDao;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class Countries {

private final CountryDao countryDao;
private final CheeseDao cheeseDao;
private final WineDao wineDao;
int idCountry= 0;


    public Countries(CountryDao countryDao, CheeseDao cheeseDao, WineDao wineDao) {
        this.countryDao = countryDao;
        this.cheeseDao = cheeseDao;
        this.wineDao = wineDao;
    }


    public List<Cheese> getCheeses(String selectCountry){
        //get the country list
        ArrayList<Country> countries  = new ArrayList<>();
        countryDao.findAll().forEach(countries::add);
        List<Cheese> cheeses = new ArrayList<>();

        //search the selected country in the list
        for (int i=0; i< countries.size(); i++){

            if(countries.get(i).getName().equals(selectCountry)){
                //get the cheese list of the selected country
                idCountry = countries.get(i).getId();
                cheeses = countries.get(i).getListCheese();

            }

        }
        if (cheeses == null){
            //if there is no cheese
            JFrame parent = new JFrame();
            JOptionPane.showMessageDialog(parent, "Il n'y a pas de fromage disponible pour ce pays ");
        }
        if (idCountry == 0){
            //if the country doesn't exist
            JFrame parent = new JFrame();
            JOptionPane.showMessageDialog(parent, "Le pays n'est pas répertorié, désolé ");

        }

        return cheeses ;

    }


    public List<Wine> getWines(String selectCountry){
        //get the country list
        ArrayList<Country> countries  = new ArrayList<>();
        countryDao.findAll().forEach(countries::add);
        List<Wine> wines = new ArrayList<>();
        //search the selected country in the list
        for (int i=0; i< countries.size(); i++){

            if(countries.get(i).getName().equals(selectCountry)){
                //get the cheese list of the selected country
                wines = countries.get(i).getListWine();
            }
        }

        if (wines == null){
            //if there is no wine
            JFrame parent = new JFrame();
            JOptionPane.showMessageDialog(parent, "Il n'y a pas de vin disponible pour ce pays ");
        }
        return wines ;

    }


}
