package fr.epf.fromagefoisgrasbaguette;

import fr.epf.fromagefoisgrasbaguette.models.Cheese;
import fr.epf.fromagefoisgrasbaguette.models.Country;
import fr.epf.fromagefoisgrasbaguette.models.Wine;
import fr.epf.fromagefoisgrasbaguette.persistence.CountryDao;
import fr.epf.fromagefoisgrasbaguette.persistence.CheeseDao;
import fr.epf.fromagefoisgrasbaguette.persistence.WineDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@EnableJpaRepositories

public class FFGBApplication {

    @Autowired
    private CheeseDao cheeseDao;

    @Autowired
    private CountryDao countryDao;

    @Autowired
    private WineDao wineDao;


    public static void main(String[] args) {

        SpringApplicationBuilder builder = new SpringApplicationBuilder(FFGBApplication.class);
        builder.headless(false).run(args);
    }


    @PostConstruct
    public void init() {

        countryDao.deleteAll();
        cheeseDao.deleteAll();
        wineDao.deleteAll();



        Cheese parmesan = new Cheese(1,"Parmesan","img/parmesan.jpg", "Le parmigiano reggiano, est l'appellation d'origine d'un fromage italien de lait de vache, fabriqué dans une zone limitée de l'Émilie-Romagne, au sud de la Lombardie.");
        Cheese camembert = new Cheese(2,"Camembert","img/camembert.jpg", "L'appellation générique camembert désigne généralement un fromage à pâte molle et à croûte fleurie. ");
        Cheese feta = new Cheese(3,"Feta","img/feta.jpg", "La feta est un fromage caillé en saumure de Grèce. Le nom de ce fromage est un emprunt à l’italien fetta (« tranche ») qui daterait du xviie siècle. ");
        Cheese roquefort = new Cheese(4,"Roquefort","img/roquefort.jpg", "De réputation internationale, il est associé à l'excellence de l'agriculture française et à sa gastronomie. ");
        Cheese emmental = new Cheese(5,"Emmental","img/emmental.jpg", "L’emmental, emmenthal ou encore emmentaler est un fromage suisse à pâte dure dont le nom provient de la vallée de l'Emme, une région à l'est du canton de Berne. ");
        Cheese fromage_blanc = new Cheese(6,"fromage_blanc","img/fromblanc.jpg", "Le fromage blanc est un fromage à pâte fraiche, obtenu par une coagulation lactique, avec ou sans une action légère de la présure, faiblement égoutté et non affiné.");
        parmesan = cheeseDao.save(parmesan);
        camembert = cheeseDao.save(camembert);
        feta = cheeseDao.save(feta);
        roquefort = cheeseDao.save(roquefort);
        emmental = cheeseDao.save(emmental);
        fromage_blanc = cheeseDao.save(fromage_blanc);

        List<Cheese> listCheese1 = new ArrayList<>();
        listCheese1.add(parmesan);
        listCheese1.add(camembert);
        listCheese1.add(feta);
        listCheese1.add(roquefort);
        listCheese1.add(emmental);
        listCheese1.add(fromage_blanc);

        List<Cheese> listCheese2 = new ArrayList<>();
        listCheese2.add(parmesan);
        listCheese2.add(camembert);
        listCheese2.add(feta);
        listCheese2.add(roquefort);
        listCheese2.add(emmental);

        List<Cheese> listCheese3 = new ArrayList<>();
        listCheese3.add(parmesan);
        listCheese3.add(camembert);
        listCheese3.add(feta);

        List<Cheese> listCheese4 = new ArrayList<>();
        listCheese4.add(parmesan);
        listCheese4.add(camembert);



        Wine bordeaux = new Wine(1, "Bordeaux","img/bordeaux.jpg", "Le vignoble de Bordeaux est le vignoble regroupant toutes les vignes du département de la Gironde, dans le Sud-Ouest de la France. ");
        Wine riesling = new Wine (2, "Riesling","img/riesling.jpg", "Le riesling est un cépage blanc, originaire de la vallée du Rhin et de la Moselle.");
        Wine sauvginon = new Wine (3, "Sauvignon","img/sauvignon.jpg","Le sauvignon B est un cépage de vigne français, très répandu en France, aux États-Unis, en Afrique du Sud, en Australie et en Nouvelle-Zélande. ");
        Wine beaujolais = new Wine (4, "Beaujolais","img/beaujolais.jpg","Le Beaujolais est une région géographique située au nord de Lyon en France, qui s'étend dans le nord du département du Rhône et dans le sud de la Saône-et-Loire." );

        bordeaux = wineDao.save(bordeaux);
        riesling =wineDao.save(riesling);
        sauvginon = wineDao.save(sauvginon);
        beaujolais = wineDao.save(beaujolais);

        List<Wine> listWine1 = new ArrayList<>();
        listWine1.add(bordeaux);
        listWine1.add(beaujolais);

        List<Wine> listWine2 = new ArrayList<>();
        listWine2.add(sauvginon);
        listWine2.add(riesling);

        List<Wine> listWine3 = new ArrayList<>();
        listWine3.add(bordeaux);
        listWine3. add(sauvginon);

        List<Wine> listWine4 = new ArrayList<>();
        listWine4.add(beaujolais);
        listWine4.add(riesling);


        Country usa = new Country(11, "United States", "img/usa.png", listCheese3,listWine1);
        Country japan = new Country(12, "Japan","img/japan.png", listCheese1, listWine2);
        Country morocco = new Country(13, "Morocco","img/morocco.png", listCheese2, listWine3);
        Country australia= new Country(14, "Australia","img/australia.png", listCheese2, listWine4);
        Country brazil = new Country(15,"Brazil","img/brazil.png", listCheese1, listWine1);
        Country germany = new Country(16, "Germany","img/germany.png", listCheese1,listWine2 );
        Country italy = new Country(17, "Italy","img/italy.png", listCheese4, listWine1);
        Country new_zealand = new Country(18, "New Zealand","img/new_zealand.png", listCheese4, listWine1);
        Country canada = new Country(19, "Canada","img/canada.png", listCheese4, listWine3);
        Country china = new Country(20, "China","img/china.png", listCheese3, listWine4);


        countryDao.save(italy);
        countryDao.save(new_zealand);
        countryDao.save(canada);
        countryDao.save(china);
        countryDao.save(usa);
        countryDao.save(japan);
        countryDao.save(morocco);
        countryDao.save(australia);
        countryDao.save(brazil);
        countryDao.save(germany);

        List<Country> listCountry = new ArrayList<>();
        listCountry.add(italy);
        listCountry.add(new_zealand);
        listCountry.add(canada);
        listCountry.add(china);
        listCountry.add(usa);
        listCountry.add(japan);
        listCountry.add(morocco);
        listCountry.add(australia);
        listCountry.add(brazil);
        listCountry.add(germany);



    }
}
