package fr.epf.fromagefoisgrasbaguette.controllers;

import fr.epf.fromagefoisgrasbaguette.Account;
import fr.epf.fromagefoisgrasbaguette.Countries;
import fr.epf.fromagefoisgrasbaguette.models.Cheese;
import fr.epf.fromagefoisgrasbaguette.models.User;
import fr.epf.fromagefoisgrasbaguette.models.Wine;
import fr.epf.fromagefoisgrasbaguette.persistence.CountryDao;
import fr.epf.fromagefoisgrasbaguette.persistence.CheeseDao;
import fr.epf.fromagefoisgrasbaguette.persistence.UserDao;
import fr.epf.fromagefoisgrasbaguette.persistence.WineDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.swing.*;
import java.io.IOException;
import java.util.List;


@Controller
public class FFGBController {
@Autowired
  private final UserDao userDao;
@Autowired
private  final CountryDao countryDao;

@Autowired
private final CheeseDao cheeseDao;

@Autowired
private final WineDao wineDao;


  public FFGBController(UserDao userDao, CountryDao countryDao, CheeseDao cheeseDao, WineDao wineDao) {
    this.userDao = userDao;
    this.countryDao = countryDao;
    this.cheeseDao = cheeseDao;
    this.wineDao = wineDao;
  }

  @GetMapping("/inscription")
  public String newUser (Model model){
    model.addAttribute("user", new User());
    return "inscription";
  }
  @PostMapping("/addusers")
  public String addUser (User user)  {
    Account account = new Account(userDao);
    String returnString = account.getInscription(user);
    return returnString;
  }
  @GetMapping("/connexion")
  public String getConneted (Model model){
    model.addAttribute("user", new User());
    return "connexion";
  }


  @PostMapping("/connexion")
  public String connexion ( User user, HttpServletRequest request) throws ServletException, IOException {
    String returnConnexion;
    Account account = new Account(userDao);
    String email = user.getEmail();
    Boolean getSession = account.getConnexion(email, user, request);
    if (getSession == true ){
        HttpSession session = request.getSession();
        session.setAttribute("user", user);
        returnConnexion = "redirect:/index";
    }
    else {
        returnConnexion = "redirect:/connexion";
    }

    return returnConnexion;
  }
  @GetMapping("/index")
  public String getIndex ( HttpServletRequest request)   {
    Account account = new Account(userDao);
      HttpSession session = request.getSession(false);
    return "index";
  }

  @GetMapping("/Italy")
  public String italy (Model model,HttpServletRequest request) {
     //check if the user is connected with session manager
      HttpSession session = request.getSession(false);
      if(session == null){
          //if not he goes back to index
          JFrame parent = new JFrame();
          JOptionPane.showMessageDialog(parent, "Vous n'êtes pas connecté");
          return "redirect:/index";
      }
      else {
          //else he gets the page
          Countries countries = new Countries(countryDao, cheeseDao, wineDao);
          List<Cheese> cheeses = countries.getCheeses("Italy");
          model.addAttribute("dataCheeses", cheeses);

         List<Wine> wines = countries.getWines("Italy" );
          model.addAttribute("dataWines", wines);

          return "./countries/italy";
      }

  }

  @GetMapping("/New Zealand")
  public String newZealand (Model model,HttpServletRequest request)   {
      HttpSession session = request.getSession(false);
      if(session == null){
          JFrame parent = new JFrame();
          JOptionPane.showMessageDialog(parent, "Vous n'êtes pas connecté");
          return "redirect:/index";
      }
      else {

      Countries countries = new Countries(countryDao, cheeseDao, wineDao);
      List<Cheese> cheeses = countries.getCheeses("New Zealand" );
      model.addAttribute("dataCheeses", cheeses);

      List<Wine> wines = countries.getWines("New Zealand" );
      model.addAttribute("dataWines", wines);

      return "./countries/new_zealand";
       }


  }

    @GetMapping("/Canada")
    public String canada (Model model,HttpServletRequest request)   {
        HttpSession session = request.getSession(false);
        if(session == null){
            JFrame parent = new JFrame();
            JOptionPane.showMessageDialog(parent, "Vous n'êtes pas connecté");
            return "redirect:/index";
        }
        else {

            Countries countries = new Countries(countryDao, cheeseDao, wineDao);
            List<Cheese> cheeses = countries.getCheeses("Canada" );
            model.addAttribute("dataCheeses", cheeses);

            List<Wine> wines = countries.getWines("Canada" );
            model.addAttribute("dataWines", wines);

            return "./countries/canada";
        }


    }

    @GetMapping("/China")
    public String china (Model model,HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if(session == null){
            JFrame parent = new JFrame();
            JOptionPane.showMessageDialog(parent, "Vous n'êtes pas connecté");
            return "redirect:/index";
        }
        else {

            Countries countries = new Countries(countryDao, cheeseDao, wineDao);
            List<Cheese> cheeses = countries.getCheeses("China" );
            model.addAttribute("dataCheeses", cheeses);

            List<Wine> wines = countries.getWines("China" );
            model.addAttribute("dataWines", wines);

            return "./countries/china";
        }


    }

    @GetMapping("/United States")
    public String unitedStates (Model model,HttpServletRequest request){
        HttpSession session = request.getSession(false);
        if(session == null){
            JFrame parent = new JFrame();
            JOptionPane.showMessageDialog(parent, "Vous n'êtes pas connecté");
            return "redirect:/index";
        }
        else {

            Countries countries = new Countries(countryDao, cheeseDao, wineDao);
            List<Cheese> cheeses = countries.getCheeses("United States" );
            model.addAttribute("dataCheeses", cheeses);

            List<Wine> wines = countries.getWines("United States" );
            model.addAttribute("dataWines", wines);

            return "./countries/unitedStates";
        }




    }

    @GetMapping("/Japan")
    public String japan (Model model,HttpServletRequest request){
        HttpSession session = request.getSession(false);
        if(session == null){
            JFrame parent = new JFrame();
            JOptionPane.showMessageDialog(parent, "Vous n'êtes pas connecté");
            return "redirect:/index";
        }
        else {

            Countries countries = new Countries(countryDao, cheeseDao, wineDao);
            List<Cheese> cheeses = countries.getCheeses("Japan" );
            model.addAttribute("dataCheeses", cheeses);

            List<Wine> wines = countries.getWines("Japan" );
            model.addAttribute("dataWines", wines);

            return "./countries/japan";
        }


    }

    @GetMapping("/Morocco")
    public String morocco (Model model,HttpServletRequest request){
        HttpSession session = request.getSession(false);
        if(session == null){
            JFrame parent = new JFrame();
            JOptionPane.showMessageDialog(parent, "Vous n'êtes pas connecté");
            return "redirect:/index";
        }
        else {

            Countries countries = new Countries(countryDao, cheeseDao, wineDao);
            List<Cheese> cheeses = countries.getCheeses("Morocco" );
            model.addAttribute("dataCheeses", cheeses);

            List<Wine> wines = countries.getWines("Morocco" );
            model.addAttribute("dataWines", wines);

            return "./countries/morocco";
        }


    }

    @GetMapping("/Australia")
    public String australia (Model model,HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if(session == null){
            JFrame parent = new JFrame();
            JOptionPane.showMessageDialog(parent, "Vous n'êtes pas connecté");
            return "redirect:/index";
        }
        else {

            Countries countries = new Countries(countryDao, cheeseDao, wineDao);
            List<Cheese> cheeses = countries.getCheeses("Australia" );
            model.addAttribute("dataCheeses", cheeses);

            List<Wine> wines = countries.getWines("Australia" );
            model.addAttribute("dataWines", wines);

            return "./countries/australia";
        }


    }
    @GetMapping("/Brazil")
    public String brazil (Model model,HttpServletRequest request  ){
        HttpSession session = request.getSession(false);
        if(session == null){
            JFrame parent = new JFrame();
            JOptionPane.showMessageDialog(parent, "Vous n'êtes pas connecté");
            return "redirect:/index";
        }
        else {

            Countries countries = new Countries(countryDao, cheeseDao, wineDao);
            List<Cheese> cheeses = countries.getCheeses("Brazil" );
            model.addAttribute("dataCheeses", cheeses);

            List<Wine> wines = countries.getWines("Brazil" );
            model.addAttribute("dataWines", wines);

            return "./countries/brazil";
        }


    }

    @GetMapping("/Germany")
    public String germany (Model model,HttpServletRequest request){
        HttpSession session = request.getSession(false);
        if(session == null){
            JFrame parent = new JFrame();
            JOptionPane.showMessageDialog(parent, "Vous n'êtes pas connecté");
            return "redirect:/index";
        }
        else {

            Countries countries = new Countries(countryDao, cheeseDao, wineDao);
            List<Cheese> cheeses = countries.getCheeses("Germany" );
            model.addAttribute("dataCheeses", cheeses);

            List<Wine> wines = countries.getWines("Germany" );
            model.addAttribute("dataWines", wines);
            
                return "./countries/germany"; 
            
          
        }


    }
  @GetMapping("/deconnexion")
  public String deconnexion( HttpServletRequest request, HttpSession session){
      session = request.getSession();
      session.removeAttribute("user");

  return "index";
  }

}


