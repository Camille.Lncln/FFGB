package fr.epf.fromagefoisgrasbaguette.models

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Cheese(@Id @GeneratedValue var id: Int? =  null, var name: String? = null, var img: String? = null, var description: String? = null){

}
