package fr.epf.fromagefoisgrasbaguette.models

import javax.persistence.*


@Entity

data class Country(@Id var id: Int? =  null, var name: String? = null, var img: String? = null, @ManyToMany var listCheese: List<Cheese>? = null, @ManyToMany var listWine: List<Wine>? = null){

}
