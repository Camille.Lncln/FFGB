package fr.epf.fromagefoisgrasbaguette.models

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class User(@Id @GeneratedValue var id: Int? =  null, var firstName: String? = null, var lastName: String?= null, var email: String?= null, var psw: String?= null, @Transient var psw2: String?= null) {

}
