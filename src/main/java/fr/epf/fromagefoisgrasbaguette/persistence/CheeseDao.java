package fr.epf.fromagefoisgrasbaguette.persistence;

import fr.epf.fromagefoisgrasbaguette.models.Cheese;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CheeseDao extends CrudRepository<Cheese, Integer> {

}
