package fr.epf.fromagefoisgrasbaguette.persistence;

import fr.epf.fromagefoisgrasbaguette.models.Country;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryDao extends CrudRepository<Country, Integer> {

}
