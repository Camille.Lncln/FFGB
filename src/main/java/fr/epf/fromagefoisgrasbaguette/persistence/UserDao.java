package fr.epf.fromagefoisgrasbaguette.persistence;

import fr.epf.fromagefoisgrasbaguette.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserDao extends CrudRepository<User, Integer> {


}
